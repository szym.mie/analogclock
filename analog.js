function transformDisplay(pos, dim) {
  return {x: (pos.x+dim.x/2), y: (-pos.y+dim.y/2)};
}


function drawClock(ctx, glows, ts, subs, rads, cols, cen, dim) {
  let art = 1 / glows;
  ctx.clearRect(0, 0, dim.x, dim.y);
  for (let a in ts) {
    let w = 4;
    let vec = calcPos(ts[a], subs[a], rads[a], cent);
    for (let g = 1; g > 0; g-=art) {
      drawArm(ctx, vec, setOpac(cols[a], g), w, dim);
      w += 8;
    }
  }
}
function calcPos(t, sub, rad, cen) {
  let ang = (t / sub) * tau;
  return [{x: cen.x, y: cen.y}, {x: Math.sin(ang) * rad, y: Math.cos(ang) * rad}];
}


function drawArm(ctx, vec, col, wid, dim) {
  ctx.beginPath();
  ctx.strokeStyle = col;
  ctx.lineWidth = wid;
  let b = transformDisplay(vec[0], dim);
  let e = transformDisplay(vec[1], dim);
  ctx.lineTo(b.x, b.y);
  ctx.lineTo(e.x, e.y);
  ctx.stroke();
}


function setOpac(col, alp) {
  let v = getRGBA(col);
  v[3] = alp;
  return setRGBA(v);
}


function getRGBA(col) {
  let v = col.split(',');
  v[0] = v[0].split('(')[1];
  v[3] = v[3].split(')')[0];
  return v;
}


function setRGBA(vals) {
  switch (vals.length) {
    case 4:
      return `rgba(${vals[0]}, ${vals[1]}, ${vals[2]}, ${vals[3]})`;
    case 3:
      return `rgba(${vals[0]}, ${vals[1]}, ${vals[2]}, 1.0)`;
    default:
        return null;
  }
}


const tau = 2 * Math.PI;
const dims = {x: 300, y: 300};
const cent = {x: 0, y: 0};
const rads = {s: 100, m: 60, h: 20};
const cols = {s: 'rgba(0, 102, 255, 1.0)', m: 'rgba(255, 51, 51, 1.0)', h: 'rgba(102, 255, 51, 1.0)'};
const subs = {s: 60, m: 60, h: 12};



  let can = document.getElementById('canvas');
  let ctx = can.getContext('2d');
  ctx.lineCap = "round";
  let d, t;
  let i = setInterval(() => {
      d = new Date();
      t = {
        s: d.getSeconds(),
        m: d.getMinutes(),
        h: d.getHours()
      };
      console.log(t);
      drawClock(ctx, 4, t, subs, rads, cols, cent, dims);
  }, 1000);
